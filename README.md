# ShorelineS

## Introduction
`ShorelineS` is a free-form coastline simulation program developed by Dano Roelvink, Professor of Coastal Engineering and Port Development at UN-IHE, Delft, the Netherlands.

The basic idea of coastline models is to describe the shoreline shape and evolution in a _quasi_-two-dimension. Yes, the shoreline is drawn (on the screen/paper) as a 2-D curve, but the shoreline coordinates are stored in a 1-D vector. As such, the shoreline model is a 1-D model.

## Using the program
The Matlab code has been organised in the following folders:
* `functions`: containing the core computational code, which should not be changed from case to case;
* `scripts`: containing the case-specific information. You should modify the code and added your data files, as has been done for the IJmuiden case here. - I made a new folder `script_test` for experimenting purposes.
* `wave2d`: auxiliary modules to compute the wave transformation from offshore to the nearshore -- this nearshore wave condition is used as the input for the ShorelineS computation.

The model can be run using MatLab or Octave. In either case, you have to specify the working directory as `shorelines/script` or `shorelines/script_test` and run the Matlab file `ShorelineS_ijmuiden.m` in the corresponding directory.

Also note that to run the program using Octave, you have to modify certain commands.